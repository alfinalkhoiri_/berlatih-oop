<?php
    require_once("animal.php");
    require_once("Ape.php");
    require_once("Frog.php");
// Shaun
    $sheep = new Animal("shaun");
    echo "Name : ". $sheep->name . "<br>"; // "shaun"
    echo "legs : ". $sheep->legs . "<br>"; // 4
    echo "cold blooded : ". $sheep->cold_blooded . "<br><br>"; // "no"
// Buduk
    $kodok = new Frog("buduk");
    echo "Name : ". $kodok->name . "<br>"; // 
    echo "legs : ". $kodok->legs . "<br>"; // 2
    echo "cold blooded : ". $kodok->cold_blooded . "<br>"; // "no"
    echo "Jump : ", $kodok->jump() . "<br><br>";
    // kera sakti
    $sungokong = new Ape("kera sakti");
    echo "Name : ". $sungokong->name . "<br>"; // 
    echo "legs : ". $sungokong->legs . "<br>"; // 4
    echo "cold blooded : ". $sungokong->cold_blooded . "<br>"; // "no"
    echo "Yell : ", $sungokong->yell() // "Auooo"
?>